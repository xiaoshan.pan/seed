# Seed Project


## Intro

This is an archetype project that utilizes the following technologies to build a server side application.

- [ ] Relational DB (In-Mem H2, PostgreSQL, Oracle)
- [ ] Spring Boot - Micro-service framework in Java
- [ ] Spring Data - JPA persistence layer that works with Hibernate 
- [ ] RESTful Controller - RESTful API
- [ ] SWAGGER UI  - Open API implementation to work with RESTful API with ease
- [ ] Maven - build/integration tools
- [ ] git - version control  

## Clone the project

```
git clone https://gitlab.com/xiaoshan.pan/seed.git
```

## Usage
Do not modify the 'main' branch directly. To play with it, create a branch from it first.