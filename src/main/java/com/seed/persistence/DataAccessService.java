package com.seed.persistence;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.seed.entity.Computer;
import com.seed.persistence.repository.ComputerRepository;

@Component
public class DataAccessService {
	private static Logger logger = LoggerFactory.getLogger(DataAccessService.class);

	/** Computer - the 'WRITE' model */
	@Autowired
	private ComputerRepository cr;

	public Computer saveComputer(Computer c) {
		logger.debug("new Computer " + c.id + " " + c.brand + " is being created.");
		return cr.save(c);
	}

	public Computer findComputerById(String id) {
		return cr.findById(id);
	}

	public List<Computer> findAllComputers() {
		return Lists.newArrayList(cr.findAll());
	}

	public void deleteComputer(Long id) {
		cr.deleteById(id);
	}
}
