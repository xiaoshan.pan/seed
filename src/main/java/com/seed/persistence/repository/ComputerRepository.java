package com.seed.persistence.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.seed.entity.Computer;

public interface ComputerRepository extends PagingAndSortingRepository<Computer, Long>{
	
	public Computer findById(String computerId);

}
