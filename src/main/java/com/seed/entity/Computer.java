package com.seed.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Computer {

	@Id
	@GeneratedValue
	public Long pid = null;

	@Basic
	public String id;

	@Basic
	public String brand;

	@OneToOne(cascade={CascadeType.ALL},fetch=FetchType.EAGER)
	public CPU cpu;

	@OneToMany(cascade={CascadeType.ALL},fetch=FetchType.EAGER)
	public Set<Disk> disks = new HashSet<Disk>();

	public Computer() {
	}

	public Computer(String id, String brand) {
		this.id = id;
		this.brand = brand;
	}

	public Computer(String id, String brand, CPU cpu, Set<Disk> disks) {
		this.id = id;
		this.brand = brand;
		this.cpu = cpu;
		this.disks = disks;
	}

	public void addDisk(Disk disk) {
		this.disks.add(disk);
	}

	public void removeDisk(Disk disk) {
		this.disks.remove(disk);
	}
}
