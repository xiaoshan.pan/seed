package com.seed.entity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Disk {
	@Id
	@GeneratedValue
	protected Long pid = null;
	
	@Basic
	private String id;
	private DiskBrand brand;
	private String description;
	public Disk() {}
	public Disk(String id, DiskBrand brand, String description) {
		super();
		this.id = id;
		this.brand = brand;
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public DiskBrand getBrand() {
		return brand;
	}
	public void setBrand(DiskBrand brand) {
		this.brand = brand;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
