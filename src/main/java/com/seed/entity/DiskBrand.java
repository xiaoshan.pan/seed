package com.seed.entity;

public enum DiskBrand {
	Western_Digital, Seagate, Toshiba, Hitachi, SanDisk, Kingston, Sony, Other
}
