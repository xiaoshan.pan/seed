package com.seed.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 
 *
 */
@Configuration
public class AppConfiguration {
	@Value("${APP_NAME:seed}")
	public String APP_NAME;
	
}
