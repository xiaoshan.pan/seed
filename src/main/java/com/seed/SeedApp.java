package com.seed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 
 *
 * Spring Boot app
 */
@SpringBootApplication
@ComponentScan("com.seed")
public class SeedApp {
	private static final Logger logger = LoggerFactory.getLogger(SeedApp.class);
	public static void main(String[] args) {
		logger.info("Hello World!");
		SpringApplication.run(SeedApp.class, args);
	}
}
