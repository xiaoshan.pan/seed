package com.seed.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.seed.entity.Computer;
import com.seed.persistence.DataAccessService;

@RestController
@RequestMapping("/computer")
public class TestController {
	@Autowired
	DataAccessService das;
	
	/*
	 * create a computer object
	 */
	@PostMapping("/computer")
	public Computer createComputer(@RequestBody Computer computer) {
		return das.saveComputer(computer);
	}
	
    @GetMapping("/computers")
    public List<Computer> findAllComputers() {
        return das.findAllComputers();
    }
    
}
